import { Component, OnInit } from '@angular/core';

import { article } from '../core/models/index';
import { DataService } from '../core/services/index';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products = [];
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.sendGetRequest().subscribe((data: article[]) => {
      console.log(data);
      this.products = data;
    });
  }

}
